// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	OpenAngle = 90.f;
	InitialYaw = 0.f;
	CurrentYaw = 0.f;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	CurrentYaw = InitialYaw;
	OpenAngle += InitialYaw;

	ActorThatOpen = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has the Open Door component on it, but no pressureplate set!"), *GetOwner()->GetName());
	}
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	float Yaw = FMath::Lerp(GetOwner()->GetActorRotation().Yaw, OpenAngle, DeltaTime + OpenDoorSpeed);
	FRotator Rotator(GetOwner()->GetActorRotation().Pitch, Yaw, GetOwner()->GetActorRotation().Roll);
	GetOwner()->SetActorRotation(Rotator);
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	float Yaw = FMath::Lerp(GetOwner()->GetActorRotation().Yaw, InitialYaw, DeltaTime + CloseDoorSpeed);
	FRotator Rotator(GetOwner()->GetActorRotation().Pitch, Yaw, GetOwner()->GetActorRotation().Roll);
	GetOwner()->SetActorRotation(Rotator);
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//UE_LOG(LogTemp, Warning, TEXT("Yaw is %f "), GetOwner()->GetActorRotation().Yaw);

	if (PressurePlate && PressurePlate->IsOverlappingActor(ActorThatOpen))
	{
		OpenDoor(DeltaTime);
		DoorLastOpened = GetWorld()->GetTimeSeconds();
		DoorCloseDelay += DeltaTime;
	}
	
	if(GetWorld()->GetTimeSeconds() >= DoorLastOpened + DoorCloseDelay)
	{
		CloseDoor(DeltaTime);
		DoorCloseDelay = 0;
	}
}

